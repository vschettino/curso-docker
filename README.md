#Introdução

Este repositório agrega os arquivos utilizados no curso Docker, DevOps e Cloud. O link para a apresentação pode ser encontrado [aqui](https://docs.google.com/presentation/d/1bx5asuC9brBk_sBvM-gcsLVAI9CBNX_mySWH8kWq4A0/edit?usp=sharing).

## Pré requisitos

1. [Docker EE](https://docs.docker.com/v17.09/engine/installation/)
1. [docker-compose](https://docs.docker.com/compose/install/)

Apesar de funcionar em [diversas versões do windows](https://success.docker.com/article/compatibility-matrix), é muito mais fácil utilizar e gerenciar a ferramenta com um hospedeiro linux. Para facilitar o dia a dia, é possível permitir a execução do [docker como usuário não administrador](https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user).

## Exemplo 0: Executando uma imagem pronta

É possível executar qualquer imagem docker no hospedeiro, sem necessidade de prévia preparação. Diversos softwares já fornecem imagens básicas no [docker hub](https://hub.docker.com/) .
```bash
docker run -p 8080:80 -d wordpress:php7.1
```


## Exemplo 1: Hello World

Conceitos: `build`, `run`, variáveis de ambiente

O objetivo é fazer uma simples aplicação "Hello World": é necessário seguir a sequência `build & run`:

```bash
docker build -t app . 
docker run app
docker run -e name=Vinicius app
```

## Exemplo 2: Mapeamento Dinâmico

Conceitos: volumes, `ps`, `exec`

Quando passamos o código para dentro da imagem (com o comando `COPY`no Dockerfile), não podemos mais alterar o conteúdo dos arquivos. Para evitar isso e facilitar o processo de desenvolvimento, podemos criar mapeamentos entre o hospedeiro e o contâiner. Assim:

 ```bash
docker build -f Dockerfile.dinamico -t app-dinamico .
docker run app-dinamico # não vai funcionar, o arquivo main.py não está lá!
docker run --name=hello --rm -v "$PWD":/hello -e name=Vinicius app-dinamico 
```

Se eu quiser executar um comando arbitrário ao executar o contâiner?

```bash
docker run -v "$PWD":/hello app-dinamico whoami
docker run -it --name=hello --rm -v "$PWD":/hello app-dinamico sh
docker exec -it hello sh
docker ps
```

## Exemplo 3: Orquestrando

Conceitos: docker-compose, configuração e integração contínua
Para fazer um teste mais completo da aplicação, você pode inserir o conteúdo da base [title.akas](https://datasets.imdbws.com/) do [IMDB](https://www.imdb.com/) em uma tabela chamada `movies`.

Para estabelecer uma aplicação, muitas vezes precisamos de diversos containers trabalhando em conjunto. O docker-compose ajuda a orquestrar e estabelecer a comunicação entre diversos componentes de um aplicação. A arquitetura é descrita através do arquivo `docker-compose.yml`.

```bash
docker-compose up --build
```

O projeto vai estar rodando na porta 8000. Você pode acessá-lo passando o identificador único de um filme, como http://localhost:8000/movie?code=tt0095016.