import os
import redis
import logging
from flask import Flask, make_response, request
import psycopg2
import json
app = Flask(__name__)


logger = logging.getLogger('MOVIES')
logging.basicConfig(level=logging.DEBUG)


@app.route('/movie')
def movie():
    # http://localhost:8000/movie?code=tt0095016
    code = request.args.get("code", "none")
    movies = get_movie(code)
    response = make_response(movies)
    response.headers['Content-Type'] = 'application/json'
    return response


def get_movie(code):
    r = redis_instance()
    if r.exists(code):
        logger.info('CACHE HIT!')
        return r.get(code)
    cur = postgres_instance()
    cur.execute("SELECT title, region FROM movies WHERE titleid = %s;", (code, ))
    movies = json.dumps(cur.fetchall())
    r.mset({code: movies})
    logger.info('POSTGRES HIT!')
    return movies


def redis_instance():
    return redis.Redis(host=os.environ['REDIS_HOST'])


def postgres_instance():
    e = os.environ
    conn = psycopg2.connect(f"dbname={e['DB_DATABASE']} user={e['DB_USER']} host={e['DB_HOST']} password={e['DB_PASSWORD']}")
    return conn.cursor()
