FROM python:3.7-alpine
MAINTAINER Vinicius Schettino <vinicius.schettino@ufjf.edu.br>
RUN mkdir /hello
WORKDIR hello
COPY ./main.py /hello
CMD python main.py